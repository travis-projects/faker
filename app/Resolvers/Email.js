// Import Email Service
const EmailService = use('App/Services/Emails')

module.exports = {
  Query: {
    email: () => EmailService.email(),
    emails: () => EmailService.emails()
  }
}

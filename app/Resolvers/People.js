// Import Email Service
const PeopleService = use('App/Services/People')

module.exports = {
  Query: {
    person: () => PeopleService.person(),
    people: () => PeopleService.people(),
    user: () => PeopleService.person(),
    users: () => PeopleService.people()
  }
}

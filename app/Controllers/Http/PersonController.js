'use strict'

// Import Email Service
const PeopleService = use('App/Services/People')

class PersonController {
  person () {
    return PeopleService.person()
  }

  persons () {
    return PeopleService.people()
  }
}

module.exports = PersonController

'use strict'

// Import Email Service
const EmailService = use('App/Services/Emails')

class EmailController {
  emails () {
    return EmailService.emails()
  }

  email () {
    return EmailService.email()
  }
}

module.exports = EmailController

'use strict'
const faker = require('faker')

const emails = {
  email () {
    const author = {
      firstname: faker.name.firstName(),
      lastname: faker.name.lastName(),
      email: faker.internet.email(),
      profileImg: faker.internet.avatar()
    }
    const message = {
      subject: faker.lorem.words(Math.floor((Math.random() * 11) + 5)),
      body: `<p>${faker.lorem.paragraphs(Math.floor((Math.random() * 7) + 1), `<br>`)}</p>`
    }
    return { author, message }
  },
  emails () {
    const emails = new Array(30).fill(null).map(e => e = this.email())
    return emails
  }
}

module.exports = emails

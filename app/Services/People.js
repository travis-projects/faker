'use strict'
const faker = require('faker')
faker.locale = 'en_GB';

const getGender = () => {
  const genders = ['Male', 'Female']
  const i = Math.floor(Math.random() * genders.length)
  return genders[i]
}


const people = {
  person () {
    const gender = getGender()
    const id = faker.random.uuid()
    const name = {
      title: faker.name.prefix(gender),
      firstname: faker.name.firstName(gender),
      lastname: faker.name.lastName(gender),
    }
    const username = faker.internet.userName(name.firstname, name.lastname)
    const email = faker.internet.email(name.firstname, name.lastname)
    const phone = faker.phone.phoneNumber()
    const profile = {
      image: faker.internet.avatar(),
      bio: faker.lorem.words(150),
      age: Math.floor(Math.random() * 81) + 5,
      gender
    }
    const location = {
      street: faker.address.streetName(),
      city: faker.address.city(),
      county: faker.address.county(),
      postcode: faker.address.zipCode(),
      country: 'United Kingdom'
    }
    return { id, name, username, email, phone, profile, location }
  },
  people () {
    const people = new Array(30).fill(null).map(e => e = this.person())
    console.log(people)
    return people
  }
}

module.exports = people

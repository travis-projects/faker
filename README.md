# CC Faker API

This is a small API, for creating fake data for use within applications that are being developed.
It comes with two flavours!

1. REST - `/rest/v1`
2. GraphQL - `/graphql`

Both have the same endpoints/queries.

## Useage

Visit the [API](https://faker.cirruscreative.host) to see the REST endpoints/GraphQL queries you can use.
You can also use [GraphiQL](https://faker.cirruscreative.host/graphql/graphiql) to browse the GraphQL queries available.

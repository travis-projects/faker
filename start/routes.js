'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')
const GraphQLServer = use('Adonis/Addons/GraphQLServer')

const graphqlAdonisOptions = {
  debug: process.env.NODE_ENV !== 'production'
}

// Default Response
Route.get('/', ({ view }) => view.render('home'))

// GraphQL endoints
Route.group(() => {
  Route.post('/', (context) => GraphQLServer.handle(context, graphqlAdonisOptions))
  Route.get('/graphiql', (context) => GraphQLServer.handleUI(context, { endpointURL: '/graphql' }))
}).prefix('/graphql')

// REST Endpoints
Route.group(() => {
  Route.get('emails', 'EmailController.emails')
  Route.get('emails/:email', 'EmailController.email')
  Route.get('persons', 'PersonController.persons')
  Route.get('persons/:person', 'PersonController.person')
  Route.get('users', 'PersonController.persons')
  Route.get('users/:user', 'PersonController.person')
}).prefix('/rest/v1')
